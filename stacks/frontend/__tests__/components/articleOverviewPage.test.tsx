jest.mock('../../src/services/periodical', () => ({
  getScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve(
    {
      author: [ { identifier: 'arbitrary_account_id' } ],
      identifier: 'arbitrary_article_id',
      isPartOf: {
        identifier: 'arbitrary_journal_slug',
        name: 'Arbitrary journal name',
      },
      name: 'Arbitrary name',
    },
  )),
}));
jest.mock('../../src/services/account', () => ({
  getProfiles: jest.fn().mockReturnValue(Promise.resolve([
    { identifier: 'arbitrary_account_id', name: 'Arbitrary name', sameAs: 'https://orcid.org/arbitrary_orcid' },
  ])),
  getSession: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary user id' })),
}));
jest.mock('../../src/services/orcid', () => ({
  getProfile: jest.fn().mockReturnValue(Promise.resolve(
    [
      { identifier: 'arbitrary_account_id', name: 'Arbitrary name', sameAs: 'https://orcid.org/arbitrary_orcid' },
      [],
    ],
 )),
  getWork: jest.fn().mockReturnValue(Promise.resolve(
    {
      author: [ { sameAs: 'https://orcid.org/arbitrary_orcid' } ],
      name: 'Arbitrary title',
    },
  )),
}));

import {
  ArticleOverviewPage,
  ArticleOverviewPageRouteParams,
} from '../../src/components/articleOverviewPage/component';
import { Spinner } from '../../src/components/spinner/component';
import { mockRouterContext } from '../mockRouterContext';

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

function initialisePage({
  articleId = 'arbitrary_article_id',
}: Partial<ArticleOverviewPageRouteParams> = {}) {
  return mount(
    <ArticleOverviewPage match={{ params: { articleId }, url: 'https://arbitrary_url' }}/>,
    mockRouterContext,
  );
}

it('should display a spinner while the article details are loading', () => {
  const page = initialisePage();

  expect(page.find(Spinner)).toExist();
});

it('should display an error when the article\'s details could not be loaded', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.reject('Arbitrary error'));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(toJson(page.find(ArticleOverviewPage))).toMatchSnapshot();
    done();
  }
});

it('should not pass a session to the article overview when no session details could be loaded', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getSession.mockReturnValueOnce(Promise.reject('Service error'));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('ArticleOverview').prop('session')).toBeUndefined();
    done();
  }
});

it('should not pass a list of author details to the article overview when they could not be loaded', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getProfiles.mockReturnValueOnce(Promise.reject('Service error'));

  const page = initialisePage();

  setImmediate(() => {
    // Article details fetched, now fetch the profiles
    page.update();
    setImmediate(checkAssertions);
  });
  function checkAssertions() {
    page.update();
    expect(page.find('ArticleOverview').prop('article').author[0].name).toBeUndefined();
    done();
  }
});

it('should not fetch account details when no article author was defined', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    author: undefined,
  }));
  const mockAccountService = require.requireMock('../../src/services/account');

  const page = initialisePage();

  setImmediate(() => {
    // Article details fetched, now fetch the profiles
    page.update();
    setImmediate(checkAssertions);
  });
  function checkAssertions() {
    page.update();
    expect(mockAccountService.getProfiles.mock.calls.length).toBe(0);
    done();
  }
});

it('should pass a list of author details to the article overview', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    author: [ { identifier: 'arbitrary_account_id' } ],
    identifier: 'arbitrary_article_id',
  }));
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getProfiles.mockReturnValueOnce(Promise.resolve([
    { identifier: 'arbitrary_account_id', name: 'Some name', sameAs: 'https://orcid.org/arbitrary_orcid' },
  ]));

  const page = initialisePage({ articleId: 'arbitrary_article_id' });

  setImmediate(() => {
    // Article details fetched, now fetch the profiles
    page.update();
    setImmediate(checkAssertions);
  });
  function checkAssertions() {
    page.update();
    expect(page.find('ArticleOverview').prop('article').author[0].name).toBe('Some name');
    done();
  }
});

it('should pass article details from ORCID if the ID is of the form <orcid>:<putcode>', (done) => {
  const mockedOrcidService = require.requireMock('../../src/services/orcid');
  mockedOrcidService.getWork.mockReturnValueOnce(Promise.resolve({
    author: [ { sameAs: 'arbitrary-orcid-url' } ],
    name: 'Some article title',
  }));
  mockedOrcidService.getProfile.mockReturnValueOnce(Promise.resolve([
    { name: 'Some author name' },
    [],
  ]));
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({}));

  const page = initialisePage({ articleId: '0000-0002-4013-9889:1337' });

  setImmediate(() => {
    page.update();
    expect(page.find('ArticleOverview').prop('article').author[0].name).toBe('Some author name');
    expect(page.find('ArticleOverview').prop('article').name).toBe('Some article title');
    done();
  });
});

it('should also pass article details from ORCID if the ORCID ID ends in an X', (done) => {
  const mockedOrcidService = require.requireMock('../../src/services/orcid');
  mockedOrcidService.getWork.mockReturnValueOnce(Promise.resolve({
    author: [ { sameAs: 'arbitrary-orcid-url' } ],
    name: 'Some article title',
  }));
  mockedOrcidService.getProfile.mockReturnValueOnce(Promise.resolve([
    { name: 'Some author name' },
    [],
  ]));
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({}));

  const page = initialisePage({ articleId: '0000-0002-4013-988X:1337' });

  setImmediate(() => {
    page.update();
    expect(page.find('ArticleOverview').prop('article').author[0].name).toBe('Some author name');
    expect(page.find('ArticleOverview').prop('article').name).toBe('Some article title');
    done();
  });
});

it('should redirect to the linked article\'s page, if an article is linked to an ORCID work', (done) => {
  const mockedOrcidService = require.requireMock('../../src/services/orcid');
  mockedOrcidService.getWork.mockReturnValueOnce(Promise.resolve({
    author: [ { sameAs: 'arbitrary-orcid-url' } ],
    name: 'Some article title',
  }));
  mockedOrcidService.getProfile.mockReturnValueOnce(Promise.resolve([
    { name: 'Some author name' },
    [],
  ]));
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    identifier: 'some-article-id',
  }));

  // WARNING: The following is really ugly!
  //          React Router's <Redirect> logs the following error:
  //          > Warning: You tried to redirect to the same route you're currently on: "/article/some-article-id"
  //          This doesn't appear when running the app, and figuring it out for the test was not worth the effort.
  //          Hence, I've simply suppressed errors for this test - my apologies.
  const prevError = console.error;
  console.error = jest.fn();

  const page = initialisePage({ articleId: '0000-0002-4013-9889:1337' });

  setImmediate(() => {
    page.update();
    console.error = prevError;

    expect(page.find('Redirect').prop('to')).toBe('/article/some-article-id');
    done();
  });
});
