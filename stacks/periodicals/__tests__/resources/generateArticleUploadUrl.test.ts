jest.mock('../../src/services/addFileToScholarlyArticle', () => ({
  addFileToScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve()),
}));
jest.mock('../../../../lib/lambda/getUploadUrl', () => ({
  getUploadUrl: jest.fn().mockReturnValue('Arbitrary URL'),
}));

import { generateArticleUploadUrl } from '../../src/resources/generateArticleUploadUrl';

const mockContext = {
  database: {} as any,
  session: { identifier: 'Arbitrary ID' },

  body: { targetCollection: { identifier: 'arbitrary_id' } },
  headers: {},
  method: 'POST' as 'POST',
  params: [ '/arbitrary_id/upload', 'arbitrary_id' ],
  path: '/arbitrary_id/upload',
  query: null,
};

beforeEach(() => {
  process.env.article_upload_bucket = 'arbitrary_bucket_name';
});

it('should error when no account could be verified', () => {
  const promise = generateArticleUploadUrl({ ...mockContext, session: new Error('Arbitrary error') });

  expect(promise).rejects.toBeInstanceOf(Error);
  expect(promise).rejects.toEqual(new Error('You do not appear to be logged in.'));
});

it('should error when no bucket to upload to was configured in the environment, and log it', () => {
  console.log = jest.fn();
  delete process.env.article_upload_bucket;

  const promise = generateArticleUploadUrl(mockContext);

  expect(promise).rejects.toBeInstanceOf(Error);
  expect(promise).rejects.toEqual(new Error('There was a problem uploading your article.'));
  expect(console.log.mock.calls.length).toBe(1);
  expect(console.log.mock.calls[0][0]).toBe('No upload bucket defined.');
});

it('should error when no article ID was specified', () => {
  const promise = generateArticleUploadUrl({
    ...mockContext,
    body: {
      result: { associatedMedia: { name: 'Arbitrary name' } },
      targetCollection: {},
    } as any,
    params: [],
    path: '/' ,
  });

  expect(promise).rejects.toBeInstanceOf(Error);
  expect(promise).rejects.toEqual(new Error('No article for which to upload a file specified.'));
});

it('should error when no filename was specified', () => {
  const promise = generateArticleUploadUrl({
    ...mockContext,
    body: {
      result: { associatedMedia: { license: 'https://creativecommons.org/licenses/by/4.0/' } },
      targetCollection: { identifier: 'Arbitrary identifier' },
    } as any,
    params: [],
    path: '/' ,
  });

  expect(promise).rejects.toBeInstanceOf(Error);
  expect(promise).rejects.toEqual(new Error('No article for which to upload a file specified.'));
});

it('should error when the user did not agree to the license', () => {
  const promise = generateArticleUploadUrl({
    ...mockContext,
    body: {
      result: { associatedMedia: { name: 'Arbitrary filename' } },
      targetCollection: { identifier: 'Arbitrary identifier' },
    } as any,
    params: [],
    path: '/' ,
  });

  expect(promise).rejects.toBeInstanceOf(Error);
  expect(promise).rejects.toEqual(new Error('Please agree to distribute the file under a CC-BY license.'));
});

it('should return the generated URL when all parameters are correct', () => {
  const mockedGetUploadUrl = require.requireMock('../../../../lib/lambda/getUploadUrl').getUploadUrl;
  mockedGetUploadUrl.mockReturnValueOnce('Some URL');

  const promise = generateArticleUploadUrl({
    ...mockContext,
    body: {
      result: {
        associatedMedia: {
          license: 'https://creativecommons.org/licenses/by/4.0/',
          name: 'Some name',
        },
      },
      targetCollection: { identifier: 'some_id' },
    },
    params: [ '/some_id/upload', 'some_id' ],
    path: '/some_id/upload',
    session: { identifier: 'Some creator' },
  });

  return expect(promise).resolves.toEqual({
    object: { toLocation: 'Some URL' },
    result: {
      associatedMedia: {
        contentUrl: expect.stringContaining('Some%20name'),
        license: 'https://creativecommons.org/licenses/by/4.0/',
        name: 'Some name',
      },
    },
  });
});
